# CurlCheck - README #
---

### Overview ###

The **CurlCheck** script can request data from an URL and displays some connection information and the received data as source and in a rendered form. The script has a simple input form and changes the protocol of the script (http/https) automatically depending on the entered URL.

### Screenshots ###

![CurlCheck - Output 1](development/readme/curlcheck1.png "CurlCheck - Output 1")

![CurlCheck - Output 2](development/readme/curlcheck2.png "CurlCheck - Output 2")

![CurlCheck - Output 3](development/readme/curlcheck3.png "CurlCheck - Output 3")

### Setup ###

* Upload the script **curlcheck.php** to your webhost.
* Customize the configuration section at the beginning of the file (optional).
* Access the script **curlcheck.php** from a webbrowser.
* Enter an URL to check and press the button **Check**.
* If the connection was successful, the information will be displayed.

### Support ###

This is a free script and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **CurlCheck** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](http://opensource.org/licenses/MIT).
