<?php


// ----0--9--8--7--6--5--4--3--2--1--1--2--3--4--5--6--7--8--9--0---- //
// ================================================================== //
//                                                                    //
//                 Curl Check - Check Curl Connection                 //
//                                                                    //
// ================================================================== //
//                                                                    //
//                      Version 1.1 / 12.05.2018                      //
//                                                                    //
//                      Copyright 2018 - PB-Soft                      //
//                                                                    //
//                           www.pb-soft.com                          //
//                                                                    //
//                           Patrick Biegel                           //
//                                                                    //
// ================================================================== //


// #####################################################################
// ##########      C O N F I G U R A T I O N - B E G I N      ##########
// #####################################################################

// Specify an example URL which is used on the start of the script. This
// URL will be displayed if the script is started and/or no URL was spe-
// cified.
$example_url = "https://www.example.com";

// Specify if the automatic redirection (http or https) is enabled. This
// option is used to switch the protocol depending on the URL entered on
// the input form.
$automatic_redirect = 1;

// Specify the user agent string for the curl connections.
$useragent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/65.0.3325.181 Chrome/65.0.3325.181 Safari/537.36";

// #####################################################################
// ############      C O N F I G U R A T I O N - E N D      ############
// #####################################################################

// Initialize the error flag.
$error = false;

// Initialize the message.
$message = "";

// Check if the form and an URL was submitted.
if (isset($_GET['url']) && !empty($_GET['url'])) {

  // Get the user input from the form.
  $input = $_GET['url'];

  // Validate the URL.
  $url = filter_var($input, FILTER_VALIDATE_URL);

  // Check if the URL is not valid.
  if(!$url) {

    // Set the error flag.
    $error = true;

    // Specify an error message.
    $message = "The input URL is not valid!";
  }

  // Check if the form was not submitted (first use).
} elseif (!isset($_GET['url'])) {

  // Set the example URL.
  $url = $example_url;

  // The user has sent the form without entering any URL.
} else {

  // Set the error flag.
  $error = true;

  // Set the example URL.
  $url = $example_url;

  // Specify an error message.
  $message = "You did not enter any URL!";
}

// Search for the remote protocol.
if (stripos($url, "https") !== false) {

  // Specify the remote protocol.
  $remote_protocol = "https://";

  // The 'https' string was not found.
} else {

  // Specify the remote protocol.
  $remote_protocol = "http://";
}

// Search for the local protocol.
if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {

  // Specify the local protocol.
  $local_protocol = "https://";

  // The 'https' string was not found.
} else {

  // Specify the local protocol.
  $local_protocol = "http://";
}

// Get the local hostname.
$local_host = $_SERVER['HTTP_HOST'];

// Specify the script URL.
$script_url = $remote_protocol.$local_host.$_SERVER['SCRIPT_NAME'];

// Check if the user has to be redirected.
if ($remote_protocol != $local_protocol && $automatic_redirect == 1) {

  // Redirect to the other protocol.
  header('Location: '.$script_url."?url=".urlencode($url));
}

?>

  <!DOCTYPE HTML>
  <html lang="en">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>URL Check with Curl</title>
      <style>
        html {color: #1d467b; font-family: Arial,Verdana,sans-serif; font-size: 10px; background-color: #f0f8ff; padding: 30px}
        body {background-color: #f0f8ff}
        input.url {font-size: 140%; color: #1d467b; background-color: #e3ecf5; margin-bottom: 10px; padding: 5px; border: 2px solid #6a8fd4; width: 100%; max-width: 1000px; -webkit-border-radius: 8px; -moz-border-radius: 8px; border-radius: 8px}
        input.submit {font-size: 140%; color: #1d467b; padding: 5px 20px}
        textarea.curl-info, textarea.curl-output {font-size: 110%; width: 100%; height: 660px; -webkit-border-radius: 8px; -moz-border-radius: 8px; border-radius: 8px}
        textarea.green {background-color: #def1dd; color: #045404; border: 2px solid #045404}
        textarea.red {background-color: #f5c1c6; color: #cc0033; border: 2px solid #cc0033}
        iframe.curl-output {height: 1040px; width: 100%; -webkit-border-radius: 8px; -moz-border-radius: 8px; border-radius: 8px}
        iframe.green {border: 2px solid #045404}
        iframe.red {border: 2px solid #cc0033}
        p.curl-error {font-size: 140%; color: #cc0033; margin-top: 0}
        h2.curl-error-title {margin-top: 50px}
        h2 {margin-bottom: 5px; margin-top: 50px}
        h2.green {color: #045404}
        h2.red {color: #cc0033}
        @media (min-width: 320px) {html {font-size: 80%} textarea.curl-info, textarea.curl-output {height: 480px}}
        @media (min-width: 480px) {html {font-size: 85%} textarea.curl-info, textarea.curl-output {height: 480px}}
        @media (min-width: 768px) {html {font-size: 90%} textarea.curl-info, textarea.curl-output {height: 500px}}
        @media (min-width: 1024px) {html {font-size: 100%} textarea.curl-info, textarea.curl-output {height: 540px}}
        @media (min-width: 1224px) {html {font-size: 110%} textarea.curl-info, textarea.curl-output {height: 630px}}
        @media (min-width: 1824px) {html {font-size: 120%} textarea.curl-info, textarea.curl-output {height: 660px}}
      </style>
    </head>
    <body>
      <h1>URL Check with Curl (<?php echo str_replace("://", "", $local_protocol); ?>)</h1>
      <p><?php if (isset($_GET['url']) && !empty($message)) { echo "<p>".$message."</p>\n"; } ?></p>
      <form method="GET" action="<?php echo $script_url; ?>">
        <h2 >Please enter an URL:</h2>
        <input class="url" type="text" name="url" value="<?php echo $url; ?>">
        <input class="submit" type="submit" value="Check">
      </form>

<?php

// Check if there was no error.
if ($error == false) {

  // Get the parts of the URL.
  $url_parts = parse_url($url);

  // Create the host string.
  $host = $url_parts['scheme']."://".$url_parts['host'];

  // Get the actual IP address of the specified host.
  $ip_address = gethostbyname($url_parts['host']);

  // Initialize a new curl resource.
  $resource = curl_init();

  // Set the URL for testing.
  curl_setopt($resource, CURLOPT_URL, $url);

  // Set the option to return the transfer data as a string.
  curl_setopt($resource, CURLOPT_RETURNTRANSFER, true);

  // Set the timeout in seconds.
  curl_setopt($resource, CURLOPT_CONNECTTIMEOUT, 30);

  // Set the user agent.
  curl_setopt($resource, CURLOPT_USERAGENT, $useragent);

  // Execute the request and save the content into a variable.
  $curl_output = curl_exec($resource);

  // Show the information title.
  echo "<h2>Actual Output is displayed for:</h2>\n";

  // Display the requested URL.
  echo "<p>Requested URL: <a href=\"".$url."\" target=\"_blank\">".$url."</a></p>\n";

  // Display the IP address.
  echo "<p>IP address for host '<strong>".$url_parts['host']."</strong>' is: <strong>".$ip_address."</strong></p>\n";

  // Check if there was no error.
  if (!curl_errno($resource)) {

    // Get some curl information.
    $curl_info = curl_getinfo($resource);

    // Check if the http code is 200.
    if ($curl_info['http_code'] == "200") {

      // Specify the CSS style.
      $style = "green";

      // The http code is not 200.
    } else {

      // Specify the CSS style.
      $style = "red";
    }

    // Display the HTTP code.
    echo "<p>Returned HTTP Code: <strong>".$curl_info['http_code']."</strong></p>\n";

    // Show the information title.
    echo "<h2 class=\"".$style."\">Curl Information</h2>\n";

    // Show the curl information.
    echo "<textarea class=\"curl-info ".$style."\">\n";
    print_r($curl_info);
    echo "</textarea>\n";

    // Check if the curl output is not empty.
    if (!empty($curl_output)) {

      // Show the output title.
      echo "<h2 class=\"".$style."\">Curl Output (Response: HTML Source)</h2>\n";

      // Show the curl output.
      echo "<textarea class=\"curl-output ".$style."\">\n";
      echo htmlspecialchars($curl_output)."\n";
      echo "</textarea>\n";

      // Show the iframe title.
      echo "<h2 class=\"".$style."\">Curl Output (Response: HTML Rendered)</h2>\n";

      // Specify the regex replacement patterns.
      $patterns = array();
      $patterns[0] = "/(?<=src=(\"|\')) ?\/(?=[a-z0-9]+)/i";
      $patterns[1] = "/(?<=srcset=(\"|\')) ?\/(?=[a-z0-9]+)/i";
      $patterns[2] = "/(?<=href=(\"|\'))\/(?=[a-z0-9]+)/i";
      $patterns[3] = "/(?<=action=(\"|\'))\/(?=[a-z0-9]+)/i";
      $patterns[4] = "/(?<=path=(\"|\'))\/(?=[a-z0-9]+)/i";
      $patterns[5] = "/(?<=content=(\"|\'))\/(?=[a-z0-9]+)/i";
      $patterns[6] = "/(?<=data-bg=(\"|\'))\/(?=[a-z0-9]+)/i";
      $patterns[7] = "/(?<=pluginAjaxCaller\(')\/(?=[a-z0-9]+)/i";

      // Change the URLs for the images/links/scripts/css.
      $curl_output = preg_replace($patterns, $host."/", $curl_output);

      // Check if the actual directory is writable.
      if (is_writable(dirname(__FILE__)) || is_writable("tmp_file.html")) {

        // Write the curl output into a temporary file.
        if (file_put_contents("tmp_file.html", $curl_output)) {

          // Show the returned output in an iframe.
          echo "<iframe src=\"tmp_file.html\" class=\"curl-output ".$style."\"></iframe>\n";

          // Permission denied for writing the temporary file.
        } else {

          // Display an error message.
          echo "<p>Could not write to the temporary file!</p>\n";
        }

        // The actual directory is not writable.
      } else {

        // Display an error message.
        echo "<p>No permission to create the temporary file!</p>\n";
      }
    } // Check if the curl output is not empty.

    // There was a curl error.
  } else {

    // Show the error title.
    echo "<h2 class=\"curl-error-title red\">Curl Error</h2>\n";

    // Show the error message.
    echo "<p class=\"curl-error red\">".curl_error($resource)."</p>\n";
  }

  // Close the curl resource.
  curl_close($resource);

} // Check if there was no error.

?>

    </body>
  </html>
